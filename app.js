let jsonfeedToRSS = require('jsonfeed-to-rss')
let fetch = require("node-fetch")

const express = require('express');

let app = express();
let port = 5005
app.use(express.urlencoded({ extended: false }));

app.use('/rss/:search_string',function(req, res, next){
    let search_string = req.params.search_string
    if(search_string){
        getJson(search_string).then(rss=>{
            res.status(200).send(rss)
        }).catch(err=>{
            res.status(500).send(err)
        })
    }else{next()}

});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404).send("Not found")
});

function getJson(search_string){
    return new Promise(function(resolve,reject){
        try{
            fetch("https://diavgeia.gov.gr/luminapi/api/search/export?q=q:[%22"+search_string+"%22]&sort=recent&wt=json")
                .then(res=>res.json())
                .then(json_data=>{
                    let json_entries_feed = json_data.decisionResultList.map(entry =>{
                        return {...entry, 
                                id:entry.ada,
                                summary:entry.subject,
                                date_published:entry.issueDate,
                                url:entry.documentUrl,
                                content_text:entry.decisionTypeUid+" "+entry.decisionTypeLabel+" "+entry.organizationUid+" "+entry.organizationLabel
                            }
                    })
                    let json_feed = {
                        "version": "https://jsonfeed.org/version/1",
                        "title": "Diavgeia RSS results for "+search_string,
                        "home_page_url": "https://crowdpolicy.com/",
                        "feed_url": "https://diavgeia.gov.gr/luminapi/api/search/export?q=q:[%22"+search_string+"%22]&sort=recent&wt=json",
                        "items": json_entries_feed
                    }
                    let rssFeed = jsonfeedToRSS(json_feed)
                    resolve(rssFeed)


                })
        }catch(error){
            reject(error)
        }
        
    })

}

app.listen(port, () => {
    console.log(`JSON to RSS listening on ${port}`)
})