FROM node:latest

ENV APP_ROOT /app

RUN mkdir $APP_ROOT
WORKDIR $APP_ROOT

COPY package.json /app/package.json

RUN npm i

COPY . /app/
	
EXPOSE 5005

CMD ["npm", "start"]